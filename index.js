const app = require("express")();
const apiKey="6fcb8550953fd4f6a72cf68a751b998f"

var https=require("https")
var http = require('http')
var moment = require('moment');
var request=require("ajax-request");
var port = process.env.PORT || 5000;

app.get("/", (req, res) => {
    //res.send("Hi edwin you can set forex levels with me!");
    res.sendFile(__dirname+"/index.html")
});

app.listen(port, () => {
    console.log("Listening on " + port);
});

http.get("http://data.fixer.io/api/latest?access_key="+apiKey, (resp) => {
  let data = '';
 
  resp.on('data', (chunk) => {
    data += chunk;
  });
 
  resp.on('end', () => {
    console.log(JSON.parse(data));
    
  });
 
}).on("error", (err) => {
  console.log("Error: " + err.message);
});
